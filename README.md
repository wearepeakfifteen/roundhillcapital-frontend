# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

Round Hill Capital Website Frontend

* Version

1.0.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

#### Summary of set up

1. `npm i`
1. `npm start`

#### Setup path for generated assets & html

Open package.json and edit `htmlfedir` and `assetdir` variables

#### Useful

* [HTML Includes](https://github.com/entozoon/html-includes#readme)

#### Configuration
#### Dependencies

* jQuery 3.1.1
* Bootstrap 5
* Swiper.js
* Modernizr
* Respond.js

#### Database configuration
#### How to run tests
#### Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

nicky@wearepeakfifteen.com

* Other community or team contact

PXV