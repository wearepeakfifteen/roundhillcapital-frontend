(function() {
$('body').on('click','.btn-scroll_to', function(e){
        
        var $target = $($(e.currentTarget).attr('href'));

        if($target.length > 0){
            var y_target = $target[0].getBoundingClientRect().top + window.scrollY;
            $.scrollTo(y_target - 130, 200);
            $target.find('a').trigger('click');
        }
        
        e.preventDefault();
        
    });
})();