(function() {
    
    window.addEventListener('load', function() { 
        AOS.init({
            anchorPlacement: 'top-bottom'
        });
    });
    
    function resizedFin(){
        setTimeout(function(){
            AOS.refresh();
        },200);
    }

    var resizeTimeout;
    $(window).resize(function(){
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(resizedFin, 100);
    });
    
})();