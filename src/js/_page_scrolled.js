(function() {

    $(window).scroll(function(e) {
        var t = $(window).scrollTop();
        if (t > 50) {
            $("html").addClass("scrolled")
        } else {
            $("html").removeClass("scrolled")
        }
    });

    $(document).ready(function(){
        var t = $(window).scrollTop();
        if (t > 50) {
            $("html").addClass("scrolled")
        } else {
            $("html").removeClass("scrolled")
        }
    });
    
})();