(function () {
    
    var teamSwipers = [];
    
	$('.section-team_contacts_swiper').each(function (i, e) {
        
        var swiperContainer = $(e).find('.swiper-container').get(0);
	    var nextEl = $(e).find('.swiper-button-next').get(0);
	    var prevEl = $(e).find('.swiper-button-prev').get(0);
        var pagEl = $(e).find('.swiper-pagination').get(0);
    
        teamSwipers[i] = new Swiper(swiperContainer, {

            slidesPerView: 1.5,
            spaceBetween: 20,
            
            breakpoints: {
                768: {
                    slidesPerView: 2
                },
                992: {
                    slidesPerView: 3
                }
            },
            
            pagination: {
                el: pagEl,
                clickable: true
            },

            navigation: {
                nextEl: nextEl,
                prevEl: prevEl,
            }

        });
        
    });
    
})();