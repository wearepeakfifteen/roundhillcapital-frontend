(function () {
    
    var testimonialsSwipers = [];
    
	$('.section-testimonials_swiper').each(function (i, e) {
        
        var swiperContainer = $(e).find('.swiper-container').get(0);
	    var nextEl = $(e).find('.swiper-button-next').get(0);
	    var prevEl = $(e).find('.swiper-button-prev').get(0);
        var pagEl = $(e).find('.swiper-pagination').get(0);
    
        testimonialsSwipers[i] = new Swiper(swiperContainer, {

            loop: true,
            slidesPerView: 1,
            centeredSlides: true,

            pagination: {
                el: pagEl,
                clickable: true
            },

            navigation: {
                nextEl: nextEl,
                prevEl: prevEl,
            }

        });
        
    });
    
})();