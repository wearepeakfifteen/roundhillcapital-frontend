(function() {
    
    var imgWTextSwipers = [];
    
	$('.section-signpost_swiper').each(function (i, e) {
        
        var swiperContainer = $(e).find('.swiper-container').get(0);
	    var nextEl = $(e).find('.swiper-button-next').get(0);
	    var prevEl = $(e).find('.swiper-button-prev').get(0);
        //var pagEl = $(e).find('.swiper-pagination').get(0);
    
        imgWTextSwipers[0] = new Swiper(swiperContainer, {

            loop: true,

            navigation: {
                nextEl: nextEl,
                prevEl: prevEl,
            },
            on: {
                slideChange: function(){
                    //bLazy.load($(e).find('.b-lazy').not('.b-loaded'));
                }
            }

        });
        
    });
    
    $('body').on('click',".section-image_w_text_slider ul.pagination-links li a", function(){
        imgWTextSwipers[0].slideTo($(this).attr('data-slide'));
        return false;
    });
    
    
    //Update home pagination on slide
    if(imgWTextSwipers.length){
	    imgWTextSwipers[0].on('slideChange', function () {
	        $(".section-image_w_text_slider ul.pagination-links li a.active").removeClass('active');
	        var thisSlide = imgWTextSwipers[0].activeIndex;
	        //Because slides are cloned, the end slide activeIndex is 1 greater than the actual slides we have
	        if(thisSlide > $(".section-image_w_text_slider ul.pagination-links li").length || thisSlide == 0){ thisSlide = 1; }
	        $(".section-image_w_text_slider ul.pagination-links li:nth-child("+thisSlide+") a").addClass('active');
	    });
    }
    
})();