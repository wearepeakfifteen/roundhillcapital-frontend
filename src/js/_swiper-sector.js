(function() {
	
	var sectorSwipers = [];
    
	$('.section-sector_swiper .tab-pane').each(function (i, e) {
        
	    var swiperContainer = $(e).children('.swiper-container').get(0);
	    var nextEl = $(e).find('.swiper-button-next').get(0);
	    var prevEl = $(e).find('.swiper-button-prev').get(0);
        var pagEl = $(e).find('.swiper-pagination').get(0);
        
	    sectorSwipers[i] = new Swiper(swiperContainer, {
            
            init: false,
	        slidesPerView: 1,
            spaceBetween: 24,
	        
            breakpoints: {
                576: {
                    slidesPerView: 1.5
                },
                768: {
                    slidesPerView: 2
                },
                1400: {
                    slidesPerView: 3
                }
            },
            
            pagination: {
                el: pagEl,
                clickable: true
            },
            
            navigation: {
                nextEl: nextEl,
                prevEl: prevEl,
            }

	    });
        
	});
    
    // Init first one 
    if(sectorSwipers.length > 0) sectorSwipers[0].init();
    
    // When a tab is changed
    $('.section-sector_swiper a[data-bs-toggle="tab"]').on('shown.bs.tab', function(e) {
        var idx = $(this).index('.section-sector_swiper a[data-bs-toggle="tab"]');
        sectorSwipers[idx].init();
    });
    
})();