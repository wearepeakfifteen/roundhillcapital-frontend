(function() {
    
    jQuery.fn.is_on_screen = function(){

        var win = $(window);

        var viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        var bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

    };
    
    var countDecimals = function (value) {
        if (Math.floor(value) !== value){
            return value.toString().split(".")[1].length || 0;
        }
        return 0;
    }
    
    function animate_stats() {
        
        $('.counter').each(function () {
                
            var $this = $(this);
            
            if($('.counter').is_on_screen() && !$this.hasClass('counted')){
                
                $this.addClass('counted');
                
                var decimals = countDecimals(parseFloat($(this).text()));
                
                jQuery({
                    Counter: 0
                }).animate({
                    Counter: $this.text()
                }, {
                    duration: 1000,
                    easing: 'swing',
                    step: function () {
                        if(decimals > 0){
                            $this.text(this.Counter.toFixed(decimals));
                        } else {
                            $this.text(Math.ceil(this.Counter));
                        }
                    }
                });
                
            }
            
        });
        
    };
    
    animate_stats();
    
    $(window).scroll(function () {
        animate_stats();
    });
    
})();