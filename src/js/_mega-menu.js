(function() {
    
    $(document).ready(function(){
        
        $('.mega-menu').on('show.bs.collapse',function(){
            $(this).addClass('showing');
            var $openmenus = $('.mega-menu.show');
            $openmenus.addClass('about-to-close');
            //setTimeout(function(){
                $openmenus.collapse('hide');
            //}, 100);
        });
        
        $('.mega-menu').on('hide.bs.collapse',function(){
            $(this).removeClass('showing').removeClass('about-to-close');
        });
        
    });
    
})();