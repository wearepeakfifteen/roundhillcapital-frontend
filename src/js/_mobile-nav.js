(function() {
    
    $(document).ready(function(){
        
        $('.navbar-collapse').on('show.bs.collapse',function(){
            $(this).addClass('showing');
        });
        
        $('.navbar-collapse').on('hide.bs.collapse',function(){
            $(this).removeClass('showing');
        });
        
    });
    
})();